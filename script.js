console.log(`hello world`)

let myTrainer = {
	name : `Ash Ketchum`,
	age : 28,
	pokemon : [`grengar`, `greninja`, `sceptile`, `lucario`],
	friends	: {
		kanto : [`Misty`, `Brook`],
		johto : [`Max`, `May`]
	},
	talk: function() {
		console.log( `Pikachu, I choose you!`)
	}
}

console.log(myTrainer)

console.log(`Result of dot notation`)
console.log(myTrainer.name)

console.log(`Result of square bracket notation`)
console.log(myTrainer[`pokemon`])

myTrainer.talk();

//Constructor

function Pokemon(name, level) {
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = 2 * level;

	this.tackle= function(target){
		console.log(`${this.name} attacked ${target.name}`)
		target.health -= this.attack
		console.log(`${target.name}'s health is now reduced to ${target.health}`)
		if(target.health <= 0){
			target.faint()
		}
	}
	this.faint= function(){
		console.log(`${this.name} has fainted`)
	}
	
}

let lucario = new Pokemon(`lucario`, 40)
let sceptile = new Pokemon(`sceptile`, 10)
let grengar = new Pokemon(`grengar`, 25)

lucario.tackle(sceptile)
sceptile.tackle(lucario)

grengar.tackle(lucario)